// InsertOne

db.rooms.insertOne ({
    name: 'single',
    accomodates: 1,
    price: 1000,
    description: 'A simple room with all the basic necessities',
    rooms_available: 10,
    isAvailable: false,
});

// InsertMany

db.rooms.insertMany ([{
    name: 'double',
    accomodates: 3,
    price: 2000,
    description: 'A room fit for a small family going on a vacation.',
    rooms_available: 5,
    isAvailable: false,
},
{
    name: 'queen',
    accomodates: 4,
    price: 4000,
    description: 'A room with a queen sized bed perfect for a simple getaway.',
    rooms_available: 15,
    isAvailable: false,
}
]);

// Find

db.rooms.find()

db.rooms.find({
    name: 'double'
});

// Update

db.rooms.updateOne(
    {
        rooms_available: 15
    },
    {
        $set: {rooms_available: 0}
    }
);

// deleteMany

db.rooms.deleteMany({
    rooms_available: 0
});